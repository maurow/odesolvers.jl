include("../src/ODEsolvers.jl")
using Base.Test
using ODEsolvers

f1(x) = (x-2).*(x-5)
jf1(x) = 2x - 7

println("Check relax parameter.")

tol = 1e-8
@test abs(newtonsolve(f1, jf1, -10; tol=tol)-2)<tol
@test abs(newtonsolve(f1, jf1, 10; tol=tol)-5)<tol

@test abs(picardsolve(f1, -10; tol=tol)-2)<tol
# doesn't work for this one: @test abs(picardsolve(f1, 10; tol=tol)-5)<tol
