module ODEsolvers

# to dispose of the @doc macro:
if VERSION<v"0.4"
    macro doc(a...)
        if length(a)>1
            a[end]
        else
            a[1].args[end]
        end
    end
end

# algebraic solvers:
include("nlsolve.jl")

# Butcher tableaus
include("btableaus.jl")

include("step_control.jl")

# integrators
include("runge_kutta.jl")
include("rosw.jl")


end
