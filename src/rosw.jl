export rosw_runner, rosw_runner_adaptive

## Tableau for Rosenbrock methods
immutable RosWTable{Name,T} <: Tableau{T}
    order::(Int...)
    A::Matrix{T}
    Γ::Matrix{T}
    bs::Matrix{T}
end
RosWTable{T}(name::Symbol,
             order::(Int...),
             A::Matrix{T},
             Γ::Matrix{T},
             bs::Matrix{T}
             ) = RosWTable{name,T}(order, A, Γ, bs)

## Transformed tableau following Jed's notation:
immutable RosWTableTrans{Name, T} <: Tableau{T}
    order::(Int...)
    Ahat::Matrix{T}
    Γii::Vector{T}
    Γinv::Matrix{T}
    bhats::Matrix{T}
end
RosWTable{T}(name::Symbol,
             Ahat::Matrix{T},
             Γii::Vector{T},
             Γinv::Matrix{T},
             bhats::Matrix{T},
             ) = RosWTable{name,T}(order, Ahat, Γii, Γinv, bhats)

function tabletransform{Name,T}(rt::RosWTable{Name,T})
    Γii = diag(rt.Γ)
    Γinv = inv(rt.Γ)
    Ahat = rt.A * Γinv
    bts = similar(rt.bs)
    bts[:,1] = squeeze(rt.bs[:,1]'*Γinv,1)
    bts[:,2] = squeeze(rt.bs[:,2]'*Γinv,1)
    return RosWTableTrans{Name,T}(rt.order, Ahat, Γii, Γinv, bts)
end

## tableau for ros34pw2
A = [0  0  0  0;
     8.7173304301691801e-01  0  0  0;
     8.4457060015369423e-01  -1.1299064236484185e-01  0  0;
     0  0  1.  0]
Γ = [4.3586652150845900e-01  0  0  0;
     -8.7173304301691801e-01  4.3586652150845900e-01  0  0;
     -9.0338057013044082e-01  5.4180672388095326e-02  4.3586652150845900e-01  0;
     2.4212380706095346e-01  -1.2232505839045147e+00  5.4526025533510214e-01  4.3586652150845900e-01]
bs = [2.4212380706095346e-01  -1.2232505839045147e+00  1.5452602553351020e+00  4.3586652150845900e-01;
      3.7810903145819369e-01  -9.6042292212423178e-02  5.0000000000000000e-01  2.1793326075422950e-01]'

ros34pw2 = RosWTable{:ros34pw2,Float64}((3,4), A, Γ, bs)
ros34pw2t = tabletransform(ros34pw2)

"""
This takes one step for a ode/dae system defined by
g(x,xdot)=0
gprime(x, xdot, α) = dg/dx + α dg/dxdot
"""     
function rosw_step(g::Function, gprime::Function, x0, dt,
                   rt::RosWTableTrans; bt_ind=1)
    stages = size(rt.Ahat,1)
    dof = length(x0)
    ys  = zeros(eltype(x0), stages, dof)

    # stage solutions
    jacobian_stale = true
    hprime_store = zeros(dof,dof)

    cij = (tril(rt.Γinv)-diagm(diag(rt.Γinv)))/dt
    for i=1:stages
        u = zeros(dof)        
        udot = zeros(dof)
        function h(yi) # length(yi)==dof
            # this function is Eq 5
            u[:] = x0 + yi
            for j=1:i-1
                for d=1:dof
                    u[d] += rt.Ahat[i,j]*ys[j,d]
                end
            end
            udot[:] = 1./(dt*rt.Γii[i]).*yi  # jed: is this index with the stage?
            for j=1:i
                # this is Eq.5-3
                for d=1:dof
                    udot[d] += cij[i,j]*ys[j,d]
                end
            end
            g(u, udot)
        end
        function hprime(yi)
            # here we only update the jacobian once per time step
            if jacobian_stale
                hprime_store[:,:] = gprime(u, udot, 1./(dt*rt.Γii[i]))  # jed: is this index with the stage?
            end
            hprime_store
        end
        # this is just a linear solve, usually
#        ys[i,:] = nlsolve(h, zeros(dof), hprime; opts=one_step_only)
        ys[i,:] = linsolve(h, hprime, zeros(dof))
#        ys[i,:] = newtonsolve(h, hprime, zeros(dof), maxsteps=1, warn=false)
        if i==1
            # calculate jacobian
            jacobian_stale = false            
        end
    end

    # completion:  (done twice for error control)
    x1 = zeros(eltype(x0), length(x0))
    for i=1:dof
        for j=1:stages
            x1[i] += rt.bhats[j,bt_ind]*ys[j,i]
        end
    end
    return x0 + x1
end

function linsolve(h, hprime, y0)
    # Does one Newton step
#    hprime*v = -h(u)
    return y0 - hprime(y0)\h(y0)
end


function rosw_runner(fn, Jfn, ts, x0::Vector; rt::RosWTable=ros34pw2)
    # vector y0
    rtt = tabletransform(rt)
    xs = Array(eltype(x0), length(x0), length(ts))
    xs[:,1] = x0'
    for i=1:length(ts)-1
        dt = ts[i+1]-ts[i]
        xs[:,i+1] = rosw_step(fn, Jfn, xs[:,i], dt, rtt)
    end
    return xs
end

################

function rosw_runner_adaptive(fn, Jfn, ts, x0::Vector; rt::RosWTable=ros34pw2,
                              reltol=1e-6, abstol=1e-6, dt0=1e-3, mindt=1e-13)
    # vector y0
    rtt = tabletransform(rt)
    ts, tstart, tend, tsgiven, tcur, xs, xcur, dof, facmax = init(x0, ts, dt0)
    dt = get_intial_step(fn, x0, dt0)
    dts = Float64[]
    xerrs = Float64[]
    xtrial = similar(x0)
    xerr = similar(x0)
    iter = 1
    steps = [0,0]  # [accepted, rejected]
    while tcur<tend
        # trial solution and its error at tcur+dt
        xtrial[:] = rosw_step(fn, Jfn, xcur, dt, rtt, bt_ind=1)
        xerr[:] = xtrial - rosw_step(fn, Jfn, xcur, dt, rtt, bt_ind=2)
        newdt = stepsize_wh96(dt, xcur, xtrial, xerr,
                              abstol, reltol, order(rtt), facmax, dof)
        if newdt>=dt # accept step if new dt is larger than old one
            push!(dts, dt)
            
            if tsgiven
                error("not_working_yet") # TODO
                # interpolate onto output points:
                f1 = fn(xtrial)
                while iter<length(ts) && ts[iter+1]<=t+dt  # output at all new times which are ≤ t+dt
                    iter += 1
                    xs[:,iter] = hermite_interp(ts[iter], t, dt, xcur, xtrial, f0, f1)
                end
            end

            # advance by the step
            xcur[:] = xtrial
            tcur += dt

            if (tcur+newdt) > (tend + dt*0.01)
                # hit end point exactly
                dt = tend-tcur
            else # just a normal step: for next step new dt is used
                dt = newdt
            end

            append!(xerrs, xerr)
            if !tsgiven
                append!(xs, xcur)
                push!(ts, tcur)
           end
            facmax = 1e5 # TODO
        elseif dt<mindt  # minimum step size reached
            @show length(xs), t, dt
            error("dt < mindt")
        else # redo step with smaller dt
            steps[2] +=1 
            dt = newdt
            facmax = 1.0 # forbids dt increases in the next step
        end
    end
    if !tsgiven
        xs = reshape(xs, dof, length(ts))
    end
    xerrs = reshape(xerrs, dof, length(dts))
    return xs, ts, steps, dts, xerrs
end
