function stepsize_hw(dt, x0, xtrial, xerr, abstol, reltol, order, facmax, dof)
    # Estimates new best step size following
    # Hairer & al 1992, p167 (with some modifications)
    order = minimum(order)
    
    # parameters to lift out of this function (TODO)
    fac = [0.8, 0.9, 0.25^(1/(order+1)), 0.38^(1/(order+1))][1]
    facmax_def = 2.0 # maximal step size increase. 1.5-5
    facmax = min(facmax_def, facmax)
    facmin = 1./facmax_def  # maximal step size decrease. ?
    
    # figure out a new step size
    tol = abstol + max(abs(x0), abs(xtrial)).*reltol # 4.10
    err = sqrt(1/dof * sum((xerr./tol).^2) )     # 4.11
#        err = maximum(abs(xerr./sc))

    newdt = dt * min(facmax, max(facmin, fac*(1/err)^(1/(order+1))))
    return newdt
end

function stepsize_wh96(dt, x0, xtrial, xerr, abstol, reltol, order, facmax, dof)
    # Estimates new best step size following
    # Wanner & Hairer 1996, p.112 (with some modifications)

    # TODO: lift parameters out of here:
    c1 = 6.
    c2 = 0.2 
    c3 = 0.9
    
    if any(isnan(xtrial))
        # reduce step size by max if solution contains NaN
        return dt*c2
    end

    order = minimum(order)
    
    tol = abstol + max(abs(x0), abs(xtrial)).*reltol
    err = maximum(abs(xerr./tol))
    # e = 0.9*(1/err)^(1/(order+1))
    # @show e
                     
    return dt * min(c1, max(c2, c3*(1/err)^(1/(order+1))) )
end


"""
Does various initialization used for adaptive steppers.  

Inputs:
- x0: IC
- ts: time steps.  Either as [start,end] or as [t0,t1,...,tend]
- dt0: initial step size guess (not implemented! TODO)

Returns:
- ts: Either [start] or  [t0,t1,...,tend]
- tstart == start
- tend = ts[end]
- tsgiven: true when ts==[t0,t1,...,tend]
- t = t0
- dt : initial step size
- xs: preallocated array if tsgiven else ==x0
- xold==x0: the last state
- dof: degrees of freedom ==length(x0)
- facmax: maximal step size increase.
"""
function init(x0, ts, dt0)
    dof = length(x0)
    # if ts is a more than a length two vector: return solution at
    # those points only
    tsgiven = length(ts)>2
    tstart = ts[1]
    tend = ts[end]
    
    if tsgiven
        ts = copy(ts) # TODO
        xs = zeros(eltype(x0), dof, length(ts))
        xs[:,1] = x0
    else
        ts = [tstart]
        xs = zeros(eltype(x0), 0)
        append!(xs, x0)
    end
    t = ts[1]
    facmax = 1.0e5 # fix magic number TODO
    xold = copy(x0)
    return ts, tstart, tend, tsgiven, t, xs, xold, dof, facmax
end

function get_intial_step(fn, y0, dt0)
    # follows Hairer et al 1992 p.169.  If dt0 is provided use that.
    if dt0!=0
        return dt0
    else
        error("not implemented")
    end    
end

