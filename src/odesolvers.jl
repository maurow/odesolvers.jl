@doc """
     delta_y = theta_scheme(odefn, tspan, y0, theta, jfn)

     Does one time step using a fixed point itteration or Newton, depending on theta and
     presence of jfn.

     theta == 0   : forward Euler
     theta == 0.5 : Crank-Nicolson 
     theta == 1   : backward Euler

     odefn: returns dy/dt
     jfn  : returns d_odefn/dy

     Notes:
     - http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/TS/TSTHETA.html#TSTHETA
      Crank-Nicolson == implicit trapezoidal rule
""" ->
function delta_y = theta_scheme(odefn, tspan, y0, theta; jfn=nothing, nltol=1e-6, nlmaxsteps=100)
    
    tol = 1e-6;
    err = 1;
    maxsteps = 40;

    h = diff(tspan);
    
    # make residual of this function zero:
    objfn(y) =  (y0 - y)/h + ( theta*odefn(tspan(2), y) + (1-theta)*odefn(tspan(1), y0))
    
    if jfn==nothing
        ynew = fixed_iter(objfn, y0; tol=nltol, maxsteps=nlmaxsteps)
    else
        jjfn(y) = theta*jfn(tspan(2), y) - eye(length(y0))/h
        ynew = newton_iter(objfn, jjfn, y0; tol=nltol, maxsteps=nlmaxsteps);
    end
    delta_y = ynew - y0;
end
