export newtonsolve, picardsolve

# @doc """
#      Finds a zero of fn using a Newton method.
#      """ -> 
function newtonsolve(fn, jfn, x0; tol=1e-8, maxsteps=100, relax=1.0, warn=true)
    xold = x0
    xnew = x0
    II = eye(length(x0))
    ii = 1
    for ii=1:maxsteps
        xnew = xold - jfn(xold)\fn(xold)
        err = norm(xnew-xold, Inf)
        if err<tol
            break
        end
        if any(isnan(xnew)) || any(isinf(xnew))
            root = xnew
            error("Bummer, non-linear solver diverged or NaN'ed...")
        end
        xold = xnew
    end

    if warn && ii==maxsteps
        warn("maxsteps exeeded")
    end
    return xnew
end


function picardsolve(fn, x0; tol=1e-8, maxsteps=100, relax=1.0)
#  picardsolve(fn, x0; tol=1e-8, maxsteps=100)
#
# Finds zero of function fn with a fixed point itteration.  This seems to work all the
# time when used in a backward Euler step, but not necessarily for other root finding.

    xold = x0
    xnew = x0
    ii=1
    for ii=1:maxsteps
        xnew = relax*fn(xold) + xold
        err = norm(xnew-xold, Inf)
        if err<tol
            break
        end
        if any(isnan(xnew)) || any(isinf(xnew))
            root = xnew
            error("Bump, non-linear solver diverged...")
        end
        xold = xnew
    end
    #disp(ii) 
    if ii==maxsteps
        warn("maxsteps exeeded")
    end
    return xnew
end
