abstract Tableau{T<:Real}
# assumes fields
    # order::Vector{T}
    # cs::Vector{T}
    # as::Matrix{T}
    # bs::Matrix{T} 


Base.eltype{T}(b::Tableau{T}) = T

# #@doc "Test whether it's an explicit method"->
isexplicit(b::Tableau) = istril(b.as)
isadaptive(b::Tableau) = size(b.bs, 1)==2
order(b::Tableau) = b.order

immutable RKTableExplicit{Name, S, T} <: Tableau{T}
    order::(Int...) # the order of the methods
    cs::Vector{T}
    as::Matrix{T}
    # one or several row vectors.  First row is used for the step,
    # second for error calc.
    bs::Matrix{T}
    function RKTableExplicit(or,cs,as,bs)
        @assert cs[1]==0
        @assert istril(as)
        @assert S==length(cs)==size(as,1)==size(as,2)==size(bs,2)
        @assert size(bs,1)==length(or)
        new(or,cs,as,bs)
    end
end
function RKTableExplicit{T}(name::Symbol, order::(Int...),
                   cs::Vector{T}, as::Matrix{T}, bs::Matrix{T})
    RKTableExplicit{name,length(cs),T}(order, cs, as, bs)
end

# A few RK tableaus
###################

# Explict
bt_feuler = RKTableExplicit(:feuler,(1,),
                             [0.],
                             zeros(1,1),
                             [1.]'
                             )
bt_midpoint = RKTableExplicit(:midpoint,(2,),
                               [0., .5],
                               [0.  0.
                                .5  0.],
                               [0., 1.]')
bt_heun = RKTableExplicit(:heun,(2,),
                           [0., 1.],
                           [0.  0.
                            1.  0.],
                           [0.5, 0.5]')
bt_rk4 = RKTableExplicit(:rk4,(4,),
                          [0.0, 0.5, 0.5, 1.0],
                          [0.0 0.0 0.0 0.0
                           0.5 0.0 0.0 0.0
                           0.0 0.5 0.0 0.0
                           0.0 0.0 1.0 0.0],
                          [1/6, 1/3, 1/3, 1/6]')

#https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta%E2%80%93Fehlberg_method
bt_rk45 = RKTableExplicit(:fehlberg,(4,5),
                   [0.0, 1/4, 3/8, 12/13, 1, 1/2],
                  [                                                  0 0 0 0 0 0
                   1/4                                                 0 0 0 0 0
                   3/32    9/32                                          0 0 0 0
                   1932/2197     -7200/2197     7296/2197                  0 0 0
                   439/216               -8     3680/513     -845/4104       0 0
                   -8/27                  2     -3544/2565   1859/4104  -11/40 0],
                   [25/216     0     1408/2565     2197/4104         -1/5     0
                    16/135     0     6656/12825     28561/56430     -9/50     2/55])
                    

#https://en.wikipedia.org/wiki/Dormand%E2%80%93Prince_method
bt_dopri5 = RKTableExplicit(:dopri,(5,4),
                     [0, 1/5, 3/10, 4/5, 8/9, 1, 1],
                     [0   0 0 0 0 0 0
                      1/5 0 0 0 0 0 0
                      3/40     9/40 0 0 0 0 0 
                      44/45     -56/15     32/9 0 0 0 0
                      19372/6561     -25360/2187     64448/6561     -212/729 0 0 0
                      9017/3168     -355/33     46732/5247     49/176     -5103/18656 0 0
                      35/384     0     500/1113     125/192     -2187/6784     11/84  0],
                     [35/384     0     500/1113     125/192     -2187/6784     11/84     0
                      5179/57600     0     7571/16695     393/640     -92097/339200     187/2100     1/40]
                     )
