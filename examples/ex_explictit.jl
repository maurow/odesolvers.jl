# Bernoulli's equation
# https://en.wikipedia.org/wiki/Bernoulli_differential_equation
include("../src/ODEsolvers.jl")
using ODEsolvers

bern(x, y) = x==0 ? 0.0 : 2y/x -x^2*y.^2

bern_sol(x, c) = x.^2./(1/5*x.^5+c)

x0 = 1.0
c = 1.0
y0 = bern_sol(x0, c)
xs = x0:0.01:(5.0+x0)
ys_exact = bern_sol(xs,c)

ys1 = rk_runner(bern, xs, y0, ODEsolvers.bt_feuler)
@show norm(ys1-ys_exact, 2)
ys2 = rk_runner(bern, xs, y0, ODEsolvers.bt_rk4)
@show norm(ys2-ys_exact, 2)
ys3 = rk_runner(bern, xs, y0, ODEsolvers.bt_dopri5)
@show norm(ys3-ys_exact, 2)

###########

x0 = 1.0
c = 1.0
y0 = [bern_sol(x0, c)]
xs = x0:0.01:(5.0+x0)
ys_exact = bern_sol(xs,c)

ys1 = rk_runner(bern, xs, y0, ODEsolvers.bt_feuler)
@show norm(ys1'-ys_exact, 2)
ys2 = rk_runner(bern, xs, y0, ODEsolvers.bt_rk4)
@show norm(ys2'-ys_exact, 2)
ys3 = rk_runner(bern, xs, y0, ODEsolvers.bt_dopri5)
@show norm(ys3'-ys_exact, 2)

# Astronomy example from Hairer et al 1992 p.129: the orbit of a
# satellite in planet moon system.

μ = 0.012277471
μ′ = 1-μ

function orbit(t, y)
    y1, y2, y1′, y2′ = y[:]
    D1 = ((y1+μ )^2 + y2^2)^(3/2)
    D2 = ((y1-μ′)^2 + y2^2)^(3/2)
    y1″ = y1 + 2*y2′ - μ′*(y1+μ)/D1 - μ*(y1-μ′)/D2
    y2″ = y2 - 2*y1′ - μ′*y2/D1     - μ*y2/D2
    
    return [y1′, y2′, y1″, y2″]
end

y0 = [0.994, 0.0, 0.0, -2.00158510637908252240537862224]
xend = 17.0652165601579625588917206249  # here the orbit should close
xs = linspace(0,xend, 1000)

# ys1 = rk_runner(orbit, xs, y0, ODEsolvers.bt_feuler)
# ys2 = rk_runner(orbit, xs, y0, ODEsolvers.bt_heun)
ys3 = rk_runner(orbit, xs, y0, ODEsolvers.bt_dopri5)
# xs = linspace(0,xend, 24_000);
# @time ys1 = rk_runner(orbit, xs, y0, ODEsolvers.bt_feuler)
# @time ys2 = rk_runner(orbit, xs, y0, ODEsolvers.bt_rk4)
xs = linspace(0,xend, 24000)
@time ys3 = rk_runner(orbit, xs, y0, ODEsolvers.bt_dopri5)
# @show norm(ys1[:,end]-y0, 2)
# @show norm(ys2[:,end]-y0, 2)
@show norm(ys3[:,end]-y0, 2)


println(" ")
println("Adaptive solver: ")
ys3a = rk_runner_adaptive(orbit, [0.,1.], y0, ODEsolvers.bt_dopri5;
                                reltol=1e-1, abstol=1e-1, dt0=xend/1000)

xs = linspace(0,xend, 1000)
@time ys3a, xs, steps, dts = rk_runner_adaptive(orbit, xs, y0, ODEsolvers.bt_dopri5;
                                           reltol=1e-9, abstol=1e-9, dt0=xend/20000)
@show steps                                           
@show norm(ys3a[:,end]-y0, 2)  # 1e-3 error needs 185 steps, compared to ~80 in Hairer ?!

@time ys3a, xs, steps, dts, xerrs = rk_runner_adaptive(orbit, xs[[1,end]], y0, ODEsolvers.bt_dopri5;
                                           reltol=1e-9, abstol=1e-9, dt0=xend/20000)


using ASCIIPlots
ASCIIPlots.lineplot(dts)


using ODE

abstol = 1e-3
reltol = 1e-3

tout, yout = ode45(orbit, y0, xs; abstol=abstol, reltol=reltol);
@time tout, yout = ode45(orbit, y0, xs; abstol=abstol, reltol=reltol);
@show norm(yout[end]-y0, 2)

@time ys3a, xs, steps, dts = rk_runner_adaptive(orbit, xs, y0, ODEsolvers.bt_dopri5;
                                           reltol=reltol, abstol=abstol, dt0=xend/20000)

@show norm(ys3a[:,end]-y0, 2)


# @show norm(yout[end]-y0, 2)
# yout = hcat(yout...);

# using Winston
# plot(yout[1,:], yout[2,:])

# xs = linspace(0,xend, 24_000);
# ys1 = rk_runner(orbit, xs, y0, ODEsolvers.bt_feuler);
# oplot(ys1[1,:], ys1[2,:],"g")

# xs = linspace(0,xend, 6000)
# ys1 = rk_runner(orbit, xs, y0, ODEsolvers.bt_rk4);
# oplot(ys1[1,:], ys1[2,:],"r")

# xs = linspace(0,xend, 6000)
# ys1 = rk_runner(orbit, xs, y0, ODEsolvers.bt_dopri5)
# oplot(ys1[1,:], ys1[2,:],"b")




nothing


