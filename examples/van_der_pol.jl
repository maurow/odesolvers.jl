# Van der Pol using DASSL.jl
include("../src/ODEsolvers.jl")
using ODE
using ODEsolvers
abstol = 1e-6
reltol = 1e-6

const mu = 100.

function vdp(t, y)
#  vdp(t, y)
#
# ydot of van der Pol equation.  (consider using rescaled eq. so
# period stays the same for different mu.  Cf. Wanner & Hairer 1991,
# p.5)
    return [y[2], mu^2*((1-y[1]^2) * y[2] - y[1])]
end
function Jvdp(t, y)
#  Jvdp(t, y, mu)
#
# Jacobian of vdp
    return [0 1;
            -mu^2*(y[2]*2*y[1] + 1)  mu^2*(1-y[1]^2)]
end

nt = 1000;
tspan = linspace(0.,2.,nt)
y0 = [2.,0.];

@time yout = ode23s(vdp, y0, tspan; jacobian=Jvdp, reltol = reltol, abstol = abstol)
yout = hcat(yout[2]...)

####
# using the RosW solver

function vdp_impl(y, ydot)
#  vdp_impl(t, y)
#
# Implicit van der Pol equation.  (consider using rescaled eq. so
# period stays the same for different mu.  Cf. Wanner & Hairer 1991,
# p.5)
    return vdp(0,y) - ydot
end
function Jvdp_impl(y, ydot, α)
# d vdp_impl /dy + α d vdp/dy
#
# Jacobian
    return Jvdp(0,y) - α * eye(2)
end

yout2 = rosw_runner(vdp_impl, Jvdp_impl, tspan, y0)

tss = 0:1e-4:2
yout22 = rosw_runner(vdp_impl, Jvdp_impl, tss, y0)
#### adaptive rosw
@time yout3, ts, steps, dts, xerrs = rosw_runner_adaptive(
                     vdp_impl, Jvdp_impl, [0., 2.], y0;
                     reltol=reltol, abstol=abstol, dt0=1e-5, mindt=1e-8)

# check
println("Abs error of ode23s vs ROSW:")
println(yout[:,end]-yout3[:,end])

# using Winston
# plot(ts, yout3[1,:])

# oplot(tspan, yout2[1,:],"r")
